$(document).ready(function(){
    const  API_URL = `https://dog.ceo/api/breeds/list/all`;

    fetch(`${API_URL}/list`)
        .then((response)=> response.json())
        .then((list)=> {
            const listaRazas = list.map((list)=> `<li>${list.all}</li>`);
            selectRazas.innerHTML = `<ul>${listaRazas}</ul>`;
        })

})